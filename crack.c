#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{

    int result = 0;
    // Hash the guess using MD5

    char * h = md5(guess, strlen(guess));
    // Compare the two hashes
    
    int test = strcmp(h,hash);
    
    if( test == 0){
        result = 1;
    }
    else{
        result = 0;
    } 
    
    // Free any malloc'd memory
    free(h); 
    
    return result;
}

// Read in a file and return the array of strings.
// Use the technique we showed in class to expand the
// array as you go.
char **readfile(char *filename)
{
    FILE * file1 = fopen(filename,"r");
    
    if(!file1){
        printf("Can't open %s for reading\n",filename);
        exit(1);
    }
       
    printf("File open\n"); 
      
    int currentsize = 0;
    int capacity = 50;
    char ** a = (char **)malloc(capacity * sizeof(char*));
    
    printf("Memory allocated\n");
    
    for (int i = 0; i < capacity; i++){
        a[i] = (char*)malloc(HASH_LEN);
    }
    
    printf("First string set memory allocated\n");
    
    while( fscanf(file1,"%s",a[currentsize]) != EOF){
        printf("%s\n",a[currentsize]);
        if (currentsize == capacity){
            capacity += 50;
            char ** temp = (char**)realloc(a, capacity * sizeof(char*));
            a = temp;
            for (int i = currentsize; i < capacity; i++){
                a[i] = (char *)malloc(HASH_LEN);
            }
        } 
        currentsize++; 
    }
        
    fclose(file1);    
        
    return a;
}


int main(int argc, char *argv[])
{
    
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }
    
    char * hashfile = argv[1];
    char * dictfile = argv[2];
    
    // Read the hash file into an array of strings
    char **hashes = readfile(hashfile);
    
    printf("Hashes file ready.\n");

    // Read the dictionary file into an array of strings
    char **dict = readfile(dictfile);
    
    printf("Dictionary file ready.\n");


    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.
    
    int i = 0;
    int j = 0;
    int result = 0;
    while(dict[i] != NULL){
        while(hashes[j] != NULL){
            result = tryguess(hashes[j],dict[i]);
            if(result == 1){
                printf("%s %s\n", hashes[j], dict[i]);               
            }
            j++;
        }
        j = 0;
        i++;
    }
    
    free(hashes);
    free(dict);
    return 0;
}
